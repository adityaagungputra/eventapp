package com.example.myeventapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GuestAdapter extends ArrayAdapter<Guest>{
	private Context context;
	private ArrayList<Guest> guests;
	private int resId;

	public GuestAdapter(Context context, int resourceId, ArrayList<Guest> objects) {
		super(context, resourceId, objects);
		resId = resourceId;
		this.context = context;
		guests = new ArrayList<Guest>();
		guests.addAll(objects);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater l = ((Activity) context).getLayoutInflater();
			row = l.inflate(resId, parent, false);
			Guest g = guests.get(position);
			TextView tv = (TextView) row.findViewById(R.id.guest_name);
			tv.setText(g.getName());
		}
		return row;
	}
}
