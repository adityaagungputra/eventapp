package com.example.myeventapp;

import java.util.Date;
import java.util.GregorianCalendar;

public class Guest {
	private String name;
	private String birth;
	public Guest(String name, String date){
		this.name = name;
		birth = date;
	}
	public String getName(){
		return name;
	}
	
	public int getBirthdate(){
		String[] parsedDate = birth.split("-");
		return Integer.parseInt(parsedDate[2]);
	}
	
	public int getBirthmonth(){
		String[] parsedDate = birth.split("-");
		return Integer.parseInt(parsedDate[1]);
	}
}
