package com.example.myeventapp;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyEvent {
	private int imageId;
	private String name;
	private Date date;

	public MyEvent(int id, String name, Date cur) {
		imageId = id;
		this.name = name;
		date = cur;
	}

	public int getImage() {
		return imageId;
	}

	public String getName() {
		return name;
	}

	public Date getDate() {
		return date;
	}

	public void setImage(int id) {
		imageId = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDate(Date d) {
		date = d;
	}

	public String getDateStr() {
		Format formatter = new SimpleDateFormat("dd MMM yyyy");
		return formatter.format(date);
	}
}
