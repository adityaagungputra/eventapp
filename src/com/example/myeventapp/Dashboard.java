package com.example.myeventapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Dashboard extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
		Bundle b = getIntent().getExtras();
		String name = b.getString("name");
		TextView txt = (TextView) findViewById(R.id.user_name);
		txt.setText(name);
		if (isPalindrome(name)){
			Toast.makeText(this, "Name is palindrome", Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(this, "Name is not palindrome", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dashboard, menu);
		return true;
	}
	
	private boolean isPalindrome(String str){
		//assumption : the alphabet in string is case insensitive
		String word = str.toLowerCase();
		int i = 0, j = word.length() - 1;
		while (i < j && word.charAt(i) == word.charAt(j)){
			i++;
			j--;
		}
		return (word.charAt(i) == word.charAt(j));
	}
	
	public void onEvent(View v){
		onPause();
		startActivityForResult(new Intent(this, EventList.class), 1);
		onResume();
	}

	public void onGuest(View v){
		onPause();
		startActivityForResult(new Intent(this, GuestList.class), 2);
		onResume();
	}
	
	private boolean isPrime(int n){
		if (n < 2){
			return false;
		} else {
			int i = 2;
			int limit = (int) Math.floor(Math.sqrt(n));
			boolean prime = true;
			while (prime && i <= limit){
				if (n % i == 0){
					prime = false;
				} else {
					i++;
				}
			}
			return prime;
		}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if (resultCode != Activity.RESULT_CANCELED){
			if (requestCode == 1 && resultCode == Activity.RESULT_OK){
				Button b = (Button) findViewById(R.id.button_event);
				b.setText(data.getStringExtra("Event"));
			} else if (requestCode == 2 && resultCode == Activity.RESULT_OK){
				Button b = (Button) findViewById(R.id.button_guest);
				b.setText(data.getStringExtra("Guest"));
				int bdate = data.getIntExtra("Birthdate", 0);
				int mon = data.getIntExtra("Month", 0);
				String str = "";
				if (bdate%2 == 0 && bdate%3 == 0){
					str = "iOS";
				} else {
					if (bdate%2 == 0) str = "blackberry";
					else if (bdate% 3 == 0) str = "android";
					else str = "feature phone";
				}
				if (isPrime(mon)){
					str += " and prime month";
				} else {
					str += " and not prime month";
				}
				Toast.makeText(this, str, Toast.LENGTH_LONG).show();
			}
		}
	}
}
