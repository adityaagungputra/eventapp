package com.example.myeventapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EventAdapter extends ArrayAdapter<MyEvent>{
	private Context context;
	private ArrayList<MyEvent> events;
	private int resId;

	public EventAdapter(Context context, int resourceId, ArrayList<MyEvent> objects) {
		super(context, resourceId, objects);
		resId = resourceId;
		this.context = context;
		this.events = new ArrayList<MyEvent>();
		this.events.addAll(objects);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater l = ((Activity) context).getLayoutInflater();
			row = l.inflate(resId, parent, false);
			MyEvent e = events.get(position);
			ImageView iv = (ImageView) row.findViewById(R.id.event_img);
			iv.setBackgroundResource(e.getImage());
			TextView name = (TextView) row.findViewById(R.id.event_name);
			name.setText(e.getName());
			TextView date = (TextView) row.findViewById(R.id.event_date);
			date.setText(e.getDateStr());
		}
		return row;
	}

}
