package com.example.myeventapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class GuestList extends Activity {
	private static final String PATH = "http://dry-sierra-6832.herokuapp.com/api/people/";
	ArrayList<Guest> guests;
	private GuestAdapter ga = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guest_list);
		if (isConnected()) {
			guests = new ArrayList<Guest>();
			new AsyncGetTask().execute(PATH);
		} else {
			new AlertDialog.Builder(this).setTitle(getText(R.string.caution))
					.setMessage(getText(R.string.internet_alert)).setCancelable(false)
					.setNeutralButton(getText(R.string.ok), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					}).show();
		}
	}

	private boolean isConnected() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		else
			return false;
	}

	private void setGuests(){
		GridView gridview = (GridView) findViewById(R.id.list_guest);
		ga = new GuestAdapter(this, R.layout.guest_view, guests);
		gridview.setAdapter(ga);
		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				// TODO Auto-generated method stub
				Guest g = ga.getItem(pos);
				Intent i = new Intent();
				i.putExtra("Guest", g.getName());
				i.putExtra("Birthdate", g.getBirthdate());
				i.putExtra("Month", g.getBirthmonth());
				setResult(Activity.RESULT_OK, i);
				finish();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.guest_list, menu);
		return true;
	}
	
	private class AsyncGetTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... args) {
			return getData(args[0]);
		}

		protected void onPostExecute(String result) {
			JSONArray ja = null;
			try {
				ja = new JSONArray(result);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			for (int i = 0; i < ja.length(); i++) {
				try {
					JSONObject o = ja.getJSONObject(i);
					guests.add(new Guest(o.getString("name"), o.getString("birthdate")));
				} catch (JSONException e) {
					// Oops
				}
			}
			setGuests();
		}

		public String getData(String url) {
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet();
			try {
				get.setURI(new URI(url));
			} catch (URISyntaxException e1) {
				e1.printStackTrace();
			}
			HttpParams params = get.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 5000);
			BufferedReader in = null;
			String result = "";
			try {
				HttpResponse r = client.execute(get);
				in = new BufferedReader(new InputStreamReader(r.getEntity().getContent()));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				String NL = System.getProperty("line.separator");
				while ((line = in.readLine()) != null){
					sb.append(line + NL);
				}
				in.close();
				result = sb.toString();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}
	}
}
