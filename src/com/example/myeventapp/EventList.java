package com.example.myeventapp;

import java.util.ArrayList;
import java.util.Date;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class EventList extends Activity {
	ArrayList<MyEvent> events = new ArrayList<MyEvent>();
	private EventAdapter ea = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_list);
		displayEvents();
	}

	private void displayEvents() {
		Date curdate = new Date();
		for (int i = 0; i < 10; i++) {
			events.add(new MyEvent(R.drawable.ic_launcher, "Event " + i, curdate));
		}
		ea = new EventAdapter(this, R.layout.event_view, events);
		ListView l = (ListView) findViewById(R.id.list_main);
		l.setAdapter(ea);
		l.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				// TODO Auto-generated method stub
				String name = ((MyEvent) ea.getItem(pos)).getName();
				Intent i = new Intent();
				i.putExtra("Event", name);
				setResult(Activity.RESULT_OK, i);
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_list, menu);
		return true;
	}

}
