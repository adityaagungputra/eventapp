package com.example.myeventapp;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class Login extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	public void onNext(View v) {
		EditText name = (EditText) findViewById(R.id.name);
		if (name.getText().toString().length() > 0) {
			Intent i = new Intent(this, Dashboard.class);
			i.putExtra("name", name.getText().toString());
			startActivity(i);
			finish();
		} else {
			new AlertDialog.Builder(this).setTitle(getText(R.string.caution)).setMessage(getText(R.string.name_alert))
					.setCancelable(false).setNeutralButton(getText(R.string.ok), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					}).show();
		}
	}

}
